package com;

import ch.gbchur.dobble.model.ICard;
import ch.gbchur.dobble.model.ICardFactory;

public class CardFactory implements ICardFactory {

    @Override
    public ICard getNewCard(int numberOfSymbols) {
        return new Card(numberOfSymbols);
    }
}
