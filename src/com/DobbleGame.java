package com;
import ch.gbchur.dobble.model.DobbleDeck;
import ch.gbchur.dobble.model.ICard;

import java.util.ArrayList;

public class DobbleGame{
    ArrayList<ICard> egal;

    public static void main(String[] args) {
        new DobbleGame().go();

    }

    private void go() {
        DobbleDeck deck = new DobbleDeck(8, new SymbolStore(), new CardFactory());
        deck.initDeck();
        deck.shuffleDeck();
        egal = deck.getCards();

        for (int i = 0; i < 2; i++) {
            egal.get(i).printCard();
        }
    }
}
