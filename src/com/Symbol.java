package com;

import ch.gbchur.dobble.model.ISymbol;

public class Symbol implements ISymbol {
    private String identifier;

    public Symbol(String identifier){
        this.identifier = identifier;
    }

    @Override
    public String getIdentifier() {
        return identifier;
    }
}
