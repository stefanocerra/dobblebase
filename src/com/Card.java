package com;

import ch.gbchur.dobble.model.ICard;
import ch.gbchur.dobble.model.ISymbol;

public class Card implements ICard {

    private int currentIndex = 0;
    private ISymbol[] symbols;

    public Card(int numberOfSymbols){
        this.symbols = new Symbol[numberOfSymbols];
    }

    @Override
    public void addSymbol(ISymbol iSymbol) {
        symbols[currentIndex] = iSymbol;
        currentIndex++;
    }

    @Override
    public void printCard() {
        for (int i = 0; i < symbols.length; i++) {
            System.out.print(symbols[i].getIdentifier() + "\t");
        }
        System.out.println("");
    }
}
