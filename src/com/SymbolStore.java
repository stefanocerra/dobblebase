package com;

import ch.gbchur.dobble.model.ISymbol;
import ch.gbchur.dobble.model.ISymbolStore;
import ch.gbchur.dobble.model.OutOfSymbolsException;

public class SymbolStore implements ISymbolStore {
    private int currentSymbol = -1;

    @Override
    public ISymbol getNextSymbol() throws OutOfSymbolsException {
        currentSymbol++;
        return new Symbol(Integer.toString(currentSymbol));
    }
}
